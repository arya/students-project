# Students Project Arya
Project : Selective control of microtubule functions

Goal : Determine how tubulin post translational modifications regulate microtubule- MAPs interactions and how this controls the cellular functions.

Methodology : Will produce MAPs using cell lysate approach and purify tubulin with controlled post translational modification. Then using the cell extract expressing my protein interest and the purified tubulin, I will perform TIRF microscopy experiments. My datas will be in .nd and .tiff format. I have to analyse these datas by quantify the intesity of signals and observing the localisation of protein and  certain phenotypes also. I will analyse dynamics of MAPs (dwell time, mean-squared displacement) using MATLAB scripts. I feed the parameters I have measured a software and these simulations will be translated into the behaviour of microtubules in living cells. Then I have to interpret the data I analysed into graphical format using graphs. 

